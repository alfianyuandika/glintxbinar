const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');
// cara jalankan node (nama file) test
/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter(i => typeof i === 'number');
}

// Should return array
function sortAscending(data) {
  // Code Here
  // 
  data = clean(data)
  let go = true
  // buat loop berulang selamanya
  while(go){
    // mulai for loop
    // kalo loop keluar, buat lagi loop
    for(let i = 0; i < data.length; i++){
      if(data[i] > data[i + 1]){
        // cek apakah nilai di array kiri lebih besar dari yang kanan
        let temp = data[i]
        // copy nilai kiri sementara
        data[i] = data[i+1]
        // nilai kiri di tukar dengan nilai kanan
        data[i+1] = temp
        // paste nilai yang di copy tadi kenanan
        break;
        // keluar dari loop
      } else if(data[i] == data[i + 1] || data[i] < data[i + 1]) {
        // kalo nilai dikiri sama dengan nilai di kanan
        // atau nilai dikiri lebih kecil dari kanan
        continue
        // lanjutkan loop
      } else {
        go =false
        // ketika tidak ada lagi nilai yang di loop
      }   
    } 
  }
  console.log(data)
  return data;
}

// Should return array
function sortDecending(data) {
  // Code Here
  data = clean(data)
  let go = true
  // yang ini sama kek di atas tapi ke balik nilai yang di cek
  while(go){
    for(let i = 0; i < data.length; i++){
      if(data[i] < data[i + 1]){
        let temp = data[i]
        data[i] = data[i+1]
        data[i+1] = temp
        break;
      } else if(data[i] == data[i + 1] || data[i] > data[i + 1]) {
        // continue
      } else {
        go =false
      }
    }
  }
  console.log(data)
  return data;
}
sortAscending(data)
sortDecending(data)
// DON'T CHANGE
test(sortAscending, sortDecending, data);
