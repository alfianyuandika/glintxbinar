const Geometry = require("./geometry");

class ThreeDimention extends Geometry {
  constructor(name) {
    super(name, "3D");
  }


  // Overridding
  introduce() {
    super.introduce();
    console.log(`This is ${this.type}!`);
  }

  calculateVolume() {
    console.log(`${this.name} Volume!`);
  }
}

module.exports = ThreeDimention;
