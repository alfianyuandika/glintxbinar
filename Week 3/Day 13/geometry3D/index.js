const Cube = require("./cube");
const Beam = require("./beam");
const Cone = require("./cone");
const Tube = require("./tube");

module.exports = { Cube, Beam, Cone, Tube };
