const ThreeDimention = require("./threeDimention");

class Cube extends ThreeDimention {
  constructor(length) {
    super("Cube");

    this.length = length
  }

  introduce() {
    super.introduce();
    console.log(`This is ${this.name}!`);
  }

  calculateVolume() {
    super.calculateVolume();
    let volume = this.length ** 3;

    console.log(`${this.name} volume is ${volume} cm3 \n`);
  }
}

module.exports = Cube;
