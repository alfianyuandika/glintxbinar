const ThreeDimention = require("./threeDimention");

class Tube extends ThreeDimention {
  constructor(radius, height) {
    super("Tube");

    this.radius = radius;
    this.height = height;
  }

  introduce() {
    super.introduce();
    console.log(`This is ${this.name}!`);
  }

  calculateVolume() {
    super.calculateVolume();
    let volume = Math.PI * (this.radius ** 2) * this.height;

    console.log(`${this.name} volume is ${volume} cm3 \n`);
  }
}

module.exports = Tube;
