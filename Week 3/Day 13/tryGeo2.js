const { Cube, Beam, Cone, Tube } = require("./geometry3D");

let cubeOne = new Cube(10);
cubeOne.calculateVolume();

let beamOne = new Beam(10, 20, 50);
beamOne.calculateVolume();

let coneOne = new Cone(10, 50);
coneOne.calculateVolume();

let tubeOne = new Tube(10, 50);
tubeOne.calculateVolume();

// let rectangleOne = new Rectangle(11, 12);
// rectangleOne.calculateArea();
// rectangleOne.calculateCircumference();
