const express = require("express"); //Import routes
const router = express.Router(); //Make a router
const HelloController = require("../controllers/helloController");

//if user go to http://localhost:3000 (GET)
router.get("/:alfian", HelloController.get);

//if user go to http://localhost:3000 (POST)
router.post("/:alfian", HelloController.post);

//if user go to http://localhost:3000 (PUT)
router.put("/:alfian", HelloController.put);

//if user go to http://localhost:3000 (DELETE)
router.delete("/:alfian", HelloController.delete);

module.exports = router;
