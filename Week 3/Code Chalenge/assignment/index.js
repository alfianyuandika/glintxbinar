const express = require("express"); //Import express
const app = express(); //Make express app
const helloRoute = require("./routes/helloRoute"); //Import helloroute

app.use("/", helloRoute)
app.listen(3000, () => console.log("Server running on 3000!"));
