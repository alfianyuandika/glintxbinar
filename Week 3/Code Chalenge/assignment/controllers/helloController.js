class HelloController {
  get(req, res) {
    console.log("This is example GET!");
    res.send(`Alfian Yuandika Putra`);
  }

  post(req, res) {
    console.log("This is example POST!");
    res.send(`Alfian Yuandika Putra`);
  }

  put(req, res) {
    console.log("This is example PUT!");
    res.send(`Alfian Yuandika Putra`);
  }

  delete(req, res) {
    console.log("This is example DELETE!");
    res.send(`Alfian Yuandika Putra`);
  }
}

module.exports = new HelloController();
