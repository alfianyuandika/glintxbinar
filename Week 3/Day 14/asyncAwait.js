//Import fs
const fs = require("fs");

//Make promise object
const readFile = (file, options) =>
  new Promise((success, failed) => {
    fs.readFile(file, options, (err, content) => {
      if (err) failed(err);
      return success(content);
    });
  });

const readAllFiles = async () => {
  try {
    let data = await Promise.all([
      readFile("./content/content1.txt", "utf-8"),
      readFile("./content/content2.txt", "utf-8"),
      readFile("./content/content3.txt", "utf-8"),
      readFile("./content/content4.txt", "utf-8"),
      readFile("./content/content5.txt", "utf-8"),
      readFile("./content/content6.txt", "utf-8"),
      readFile("./content/content7.txt", "utf-8"),
      readFile("./content/content8.txt", "utf-8"),
      readFile("./content/content9.txt", "utf-8"),
      readFile("./content/content10.txt", "utf-8"),
    ]);
    
    let content = data[0] + data[1]+ data[2]+ data[3]+ data[4]+ data[5]+ data[6]+ data[7]+ data[8]+ data[9];

    console.log(content);
  } catch (e) {
    console.error("It's error");
  }
};
readAllFiles();
